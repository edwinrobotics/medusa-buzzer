# Medusa Buzzer

![Medusa Buzzer](https://shop.edwinrobotics.com/4065-thickbox_default/medusa-buzzer.jpg "Medusa Buzzer")

[Medusa Buzzer](https://shop.edwinrobotics.com/modules/1214-medusa-buzzer.html)

This easy to plug in Buzzer from the medusa line makes it an ideal choice when it comes to adding an extra element of sound to any project. Using a digital output on this piezo base module , it can be programmed to emit tone when output is high, or connect to an analog pulse width modulation to generate variety of tones and cool effects. 


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.